#!/bin/bash

# exit if a command returns a non-zero exit code
# print the commands and their args as they are executed
set -e -x

# Add the pulumi CLI to PATH
export PATH=$PATH:$HOME/.pulumi/bin

pulumi stack select flask

# make deployment
pulumi up --yes
