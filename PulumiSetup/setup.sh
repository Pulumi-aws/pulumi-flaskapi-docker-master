#!/bin/bash

# exit if a command returns a non-zero exit code
# print the commands and their args as they are executed
set -e -x

# Download and install required tools
# update the GitLab Runner and install other packages
apt-get update -y
apt-get install sudo python3-pip python3-venv unzip wget curl -y

curl -fsSL https://get.pulumi.com/ | bash
export PATH=$PATH:$HOME/.pulumi/bin
export PULUMI_ACCESS_TOKEN=$PULUMI_ACCESS_TOKEN

# Login into pulumi. This will require the PULUMI_ACCESS_TOKEN environment variable
# pulumi config set --secret pulumi-access-token $PULUMI_ACCESS_TOKEN
pulumi login

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip -qq awscliv2.zip
sudo ./aws/install
#sudo su

