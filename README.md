AWS :cloud:, Pulumi :wrench:, Python :snake: and CI/CD :fox:

**Intro**
This repository create a Flaks api using Docker, Fargate and ECS. The `Dockerfile` and `app.py` files with `requirment.txt` are file that can create our flask api. The difference between this project and my previous project [Creating api using pulumi and Docker](https://gitlab.com/ehsan.tafehi/pulumi-docker-api.git) is that in previous project the Docker image was created using pulumi library called pulumi-docker. However, in this project the docker image is created inside git repository and saved on gitlab registry. Then this file will be pushed into Fargate and finally cretaed fargates will be controlled by ECS. This api can be tested using `postman`. By posting two numbers this api can do /Add, /Multiply, /Subtract and /Divide. The mani page explain the infrastructure. Copy and paste the URL created via pulumi and test the api as in **postman**

                                                                        **postman**


![postman](postman.PNG)

**The structure**:
- Pushing all data into Gitlab and creating a pipeline using `.gitlab-ci.yml`.
- Creating a Docker image using Dockerfile in Gitlab and saving docker image on gitlab registry. Then the created image will be pushed into Fargates.
- Creating a load balancer on port 80 to watch the incoming traffic.
- Pushing the created image into Amazone Fargates.
- Using ECS orchestrator on top of fargates.
- lunching the infrastructure and api is ready to useas in **postman**


                                                                        **Structure**

![Structure](Structure.PNG)

**Architecture**:
- The api is created via Flask in Pycharm :snake:
- The docker image created by Gitlab CI/CD pipeline is pushed into AWS Fargate.
- For post method only it takes 2 numbwers as input as show the `./Add`, `./Multiply`, `./Subtract` and `./Divide`
- initially it installs Pulumi, Python and awscli.
- The the pipeline preview the whole VPC infrastructure.
- if the preview is successful then at the next stage of the pipeline it runs and deploy the api on AWS.
                                        **Architecture**

![Architecture](architecture.png)


**Notes:** :speech_balloon:

I have used AWS account for the purpuse of this project.

Feel free to run `pulumi destroy --yes` command to delete all of deployed infrastructure.

the whole IaC steps are automatized. Only AWS credentials need to be set on gitLab in order for the pipeline to run successfully
