FROM python:alpine3.7
COPY . /app
WORKDIR /app
RUN pip3 install -r requirement.txt
EXPOSE 80:80
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]
